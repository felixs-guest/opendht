Source: opendht
Section: libs
Priority: optional
Maintainer: Alexandre Viau <aviau@debian.org>
Build-Depends: cython3,
               debhelper (>= 10),
               dh-exec,
               dh-python,
               libargon2-0-dev,
               libgnutls28-dev,
               libmsgpack-dev (>= 1.2),
               libncurses5-dev,
               libpython3-dev,
               libreadline6-dev,
               pkg-config
Standards-Version: 4.5.0
Homepage: https://github.com/savoirfairelinux/opendht
Vcs-Git: https://salsa.debian.org/debian/opendht.git
Vcs-Browser: https://salsa.debian.org/debian/opendht
Rules-Requires-Root: binary-targets

Package: libopendht-dev
Architecture: any
Section: libdevel
Depends: libargon2-0-dev,
         libgnutls28-dev,
         libmsgpack-dev (>= 1.2),
         libncurses5-dev,
         libreadline6-dev,
         ${misc:Depends}
Provides: libopendht-dev
Breaks: libopendht1 (<< 1.2.1~dfsg1-6)
Replaces: libopendht1 (<< 1.2.1~dfsg1-6)
Multi-Arch: same
Description: Development files for the libopendht library
 OpenDHT is a C++14 distributed hash table implementation.
 Feature list:
  - Distributed shared key->value data-store
  - Clean and powerful distributed map API with storage of
    arbitrary binary values of up to 56 KB.
  - Optional public key cryptography layer providing data
    signature and encryption (using GnuTLS).
  - IPv4 and IPv6 support.
 .
 This package contains the static library and headers.

Package: libopendht2
Architecture: any
Section: libdevel
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: Shared library for the libopendht library
 OpenDHT is a C++14 distributed hash table implementation.
 Feature list:
  - Distributed shared key->value data-store
  - Clean and powerful distributed map API with storage of
    arbitrary binary values of up to 56 KB.
  - Optional public key cryptography layer providing data
    signature and encryption (using GnuTLS).
  - IPv4 and IPv6 support.
 .
 This package contains the shared library.

Package: dhtnode
Architecture: any
Section: net
Depends: adduser, ${misc:Depends}, ${shlibs:Depends}
Description: OpenDHT node binary
 OpenDHT is a C++14 distributed hash table implementation.
 Feature list:
  - Distributed shared key->value data-store
  - Clean and powerful distributed map API with storage of
    arbitrary binary values of up to 56 KB.
  - Optional public key cryptography layer providing data
    signature and encryption (using GnuTLS).
  - IPv4 and IPv6 support.
 .
 This package contains the OpenDHT node binary.

Package: python3-opendht
Architecture: any
Section: net
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Description: OpenDHT node binary
 OpenDHT is a C++14 distributed hash table implementation.
 Feature list:
  - Distributed shared key->value data-store
  - Clean and powerful distributed map API with storage of
    arbitrary binary values of up to 56 KB.
  - Optional public key cryptography layer providing data
    signature and encryption (using GnuTLS).
  - IPv4 and IPv6 support.
 .
 This package contains Python3 bindings to OpenDHT
